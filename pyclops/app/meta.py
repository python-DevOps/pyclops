'''
This module contains meta classes specific to pyclops.app package
'''
__created__ = "2015-10-07"
__updated__ = "2015-10-08"
__author__ = "dave"
from pyclops.utils.meta import StructMeta


class AppMeta(StructMeta):
    '''
    Meta class for Apps
    '''
    pass