'''
This is the main set of classes for use in defining a pyclops appliction
'''
__created__ = "2015-10-04"
__updated__ = "2015-10-08"
__author__ = "dave"
from pyclops.app.descriptors import ApplicationDescription, ApplicationExposure, ApplicationName, ApplicationSettings, ApplicationDependancies
from pyclops.app.meta import AppMeta


class App(metaclass=AppMeta):
    '''
    This is the App class it is used for creating and registering Apps
    '''

    name = ApplicationName()
    description = ApplicationDescription()
    expose = ApplicationExposure()
    dependancies = ApplicationDependancies()
    settings = ApplicationSettings()

    def __init__(self, *args, **kwargs):
        print(kwargs)
        bound = self.__signature__.bind(*args, **kwargs)
        for name, val in bound.arguments.items():
            print(name, val)
            setattr(self, name, val)
