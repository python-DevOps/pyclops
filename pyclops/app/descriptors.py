from pyclops.utils.descriptors.base import Descriptor, StructDescriptor
from pyclops.utils.descriptors.primative import Bool
from pyclops.utils.descriptors.simple import SizedString, PosInteger, SizedRegexString
from pyclops.utils.meta import StructMeta


class TCPPort(PosInteger):
    '''
    A descriptor for validating a valid TCP port
    '''

    def __set__(self, instance, value):
        if value > 65535:
            raise ValueError('Expected <= 65535')
        super().__set__(instance, value)


class HostName(SizedRegexString):
    '''
    A descriptor for validating hostnames and IP addresses
    '''

    # we don't bother actually using _ip_v4_regex here because _host_regex catches it all
    #_ipv4_regex = '(?:^|\s)((?:25[0-5]|2[0-4]\d|[01]?\d\d?)\.(?:25[0-5]|2[0-4]\d|[01]?\d\d?)\.(?:25[0-5]|2[0-4]\d|[01]?\d\d?)\.(?:25[0-5]|2[0-4]\d|[01]?\d\d?))'
    # leave this here incase we decide to need or use an ipv4_regex

    _ipv6_regex = '^(^(([0-9A-Fa-f]{1,4}(((:[0-9A-Fa-f]{1,4}){5}::[0-9A-Fa-f]{1,4})|((:[0-9A-Fa-f]{1,4}){4}::[0-9A-Fa-f]{1,4}(:[0-9A-Fa-f]{1,4}){0,1})|((:[0-9A-Fa-f]{1,4}){3}::[0-9A-Fa-f]{1,4}(:[0-9A-Fa-f]{1,4}){0,2})|((:[0-9A-Fa-f]{1,4}){2}::[0-9A-Fa-f]{1,4}(:[0-9A-Fa-f]{1,4}){0,3})|(:[0-9A-Fa-f]{1,4}::[0-9A-Fa-f]{1,4}(:[0-9A-Fa-f]{1,4}){0,4})|(::[0-9A-Fa-f]{1,4}(:[0-9A-Fa-f]{1,4}){0,5})|(:[0-9A-Fa-f]{1,4}){7}))$|^(::[0-9A-Fa-f]{1,4}(:[0-9A-Fa-f]{1,4}){0,6})$)|^::$)|^((([0-9A-Fa-f]{1,4}(((:[0-9A-Fa-f]{1,4}){3}::([0-9A-Fa-f]{1,4}){1})|((:[0-9A-Fa-f]{1,4}){2}::[0-9A-Fa-f]{1,4}(:[0-9A-Fa-f]{1,4}){0,1})|((:[0-9A-Fa-f]{1,4}){1}::[0-9A-Fa-f]{1,4}(:[0-9A-Fa-f]{1,4}){0,2})|(::[0-9A-Fa-f]{1,4}(:[0-9A-Fa-f]{1,4}){0,3})|((:[0-9A-Fa-f]{1,4}){0,5})))|([:]{2}[0-9A-Fa-f]{1,4}(:[0-9A-Fa-f]{1,4}){0,4})):|::)((25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{0,2})\.){3}(25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{0,2})$$'
    _host_regex = '^[a-zA-Z0-9]+([a-zA-Z0-9\-\.]+)'

    _patterns = [_ipv6_regex, _host_regex]

    def __init__(self, *args, maxlen=None, **kwargs):
        super().__init__(*args, maxlen=255, patterns=self._patterns, ** kwargs)


class ApplicationName(SizedString):
    '''
    A descriptor for validating Application Names
    '''
    pass


class ApplicationDependancies(Descriptor):
    '''
    A descriptor for validating Application Dependancies
    '''
    pass


class ApplicationDescription(SizedString):
    '''
    A descriptor for validating Application Descriptions
    '''

    def __init__(self, *args, maxlen=None, **kwargs):
        super().__init__(*args, maxlen=500, **kwargs)


class ApplicationExposure(StructDescriptor, metaclass=StructMeta):
    '''
    a descriptor class that provides a series of properties
    for describing how an application is exposed
    '''

    port = TCPPort()
    host_name = HostName()
    public = Bool()
    ssl = Bool()

    def __init__(self, *args, **kwargs):
        if kwargs:
            bound = self.__signature__.bind(*args, **kwargs)
            for name, val in bound.arguments.items():
                setattr(self, name, val)


class ApplicationSettings(SizedString):
    '''
    '''
    pass
