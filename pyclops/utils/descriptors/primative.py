'''
primitive descriptors
'''
__created__ = "2015-10-07"
__updated__ = "2015-10-08"
__author__ = "dave"


from pyclops.utils.descriptors.base import Typed


class Bool(Typed):
    '''
    descriptor for creating Integer Type checked properties of a "struct"
    '''
    ty = bool


class Integer(Typed):
    '''
    descriptor for creating Integer Type checked properties of a "struct"
    credit to David Beazley (@dabeaz) and his talk "Python 3 Meta Programming" http://www.dabeaz.com/py3meta
    '''
    ty = int


class Float(Typed):
    '''
    descriptor for creating Float Type checked properties of a "struct"
    credit to David Beazley (@dabeaz) and his talk "Python 3 Meta Programming" http://www.dabeaz.com/py3meta
    '''
    ty = float


class String(Typed):
    '''
    descriptor for creating String Type checked properties of a "struct"
    credit to David Beazley (@dabeaz) and his talk "Python 3 Meta Programming" http://www.dabeaz.com/py3meta
    '''
    ty = str
