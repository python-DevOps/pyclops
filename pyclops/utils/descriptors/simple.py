'''
simple descriptors builds on primitives
'''
__created__ = "2015-10-07"
__updated__ = "2015-10-08"
__author__ = "dave"
import re

from pyclops.utils.descriptors import Descriptor
from pyclops.utils.descriptors.primative import Integer, Float, String


class Positive(Descriptor):
    '''
    A descriptor for validating for positive numbers for class properties
    credit to David Beazley (@dabeaz) and his talk "Python 3 Meta Programming" http://www.dabeaz.com/py3meta
    '''

    def __set__(self, instance, value):
        if value < 0:
            raise ValueError('Expected >= 0')
        super().__set__(instance, value)


class PosInteger(Integer, Positive):
    '''
    A descriptor for validating positive integers for class properties
    credit to David Beazley (@dabeaz) and his talk "Python 3 Meta Programming" http://www.dabeaz.com/py3meta
    '''
    pass


class PosFloat(Float, Positive):
    '''
    A descriptor for validating positive floats for class properties
    credit to David Beazley (@dabeaz) and his talk "Python 3 Meta Programming" http://www.dabeaz.com/py3meta
    '''
    pass


class Sized(Descriptor):
    '''
    A descriptor for validating the size of a property value for any type implementing __len__
    credit to David Beazley (@dabeaz) and his talk "Python 3 Meta Programming" http://www.dabeaz.com/py3meta
    '''

    def __init__(self, *args, maxlen=None, **kwargs):
        if maxlen == None:
            maxlen = 80
        self.maxlen = maxlen
        super().__init__(*args, **kwargs)

    def __set__(self, instance, value):
        if len(value) > self.maxlen:
            raise ValueError('Too big')
        super().__set__(instance, value)


class SizedString(String, Sized):
    '''
    A descriptor for validating strings for class properties
    credit to David Beazley (@dabeaz) and his talk "Python 3 Meta Programming" http://www.dabeaz.com/py3meta
    '''
    pass


class Regex(Descriptor):
    '''
    A descriptor for validating regex matches on a class property
    credit to David Beazley (@dabeaz) and his talk "Python 3 Meta Programming" http://www.dabeaz.com/py3meta
    extended by @dave to run on a list of patterns
    '''

    def __init__(self, *args, patterns, **kwargs):
        if type(patterns) != list:
            patterns = [patterns]
        self.patterns = map(re.compile, patterns)
        super().__init__(*args, **kwargs)

    def __set__(self, instance, value):
        for pattern in self.patterns:
            if pattern.match(value):
                break
        else:
            raise ValueError('Invalid string')
        super().__set__(instance, value)


class SizedRegexString(SizedString, Regex):
    '''
    A descriptor for validating regex matches, string type and length for class properties
    credit to David Beazley (@dabeaz) and his talk "Python 3 Meta Programming" http://www.dabeaz.com/py3meta
    '''
    pass
