'''
base set of descriptor classes
'''
__created__ = "2015-10-07"
__updated__ = "2015-10-08"
__author__ = "dave"


class Descriptor:
    '''
    base descriptor class all other descriptors should inherit from
    credit to David Beazley (@dabeaz) and his talk "Python 3 Meta Programming" http://www.dabeaz.com/py3meta
    '''

    def __init__(self, name=None):
        self.name = name

    def __set__(self, instance, value):
        instance.__dict__[self.name] = value

    def __delete__(self, instance):
        raise AttributeError("Can't delete")


class StructDescriptor(Descriptor):
    '''
    base descriptor class all other descriptors should inherit from
    credit to David Beazley (@dabeaz) and his talk "Python 3 Meta Programming" http://www.dabeaz.com/py3meta
    '''

    def __set__(self, instance, value):
        print(value)
        print(self.__class__)
        instance.__dict__[self.name] = self.__class__(**value)


class Typed(Descriptor):
    '''
    descriptor for creating Type checked properties of a "struct"
    credit to David Beazley (@dabeaz) and his talk "Python 3 Meta Programming" http://www.dabeaz.com/py3meta
    '''
    ty = object

    def __set__(self, instance, value):
        if not isinstance(value, self.ty):
            raise TypeError('Expected %s' % self.ty)
        super().__set__(instance, value)

# Specialized types
