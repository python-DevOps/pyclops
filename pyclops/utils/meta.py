'''
base meta classes for  use in pyclops
'''
__created__ = "2015-10-07"
__updated__ = "2015-10-07"
__author__ = "dave"
from collections import OrderedDict

from pyclops.utils.descriptors import Descriptor
from pyclops.utils.helpers import make_signature


class StructMeta(type):
    '''
    A class to be used as a meta class that provides signature creation and __init__ func construction 
    credit to David Beazley (@dabeaz) and his talk "Python 3 Meta Programming" http://www.dabeaz.com/py3meta
    '''

    @classmethod
    def __prepare__(cls, name, bases):
        return OrderedDict()

    def __new__(cls, clsname, bases, clsdict):
        fields = [key for key, val in clsdict.items()
                  if isinstance(val, Descriptor)]
        for name in fields:
            clsdict[name].name = name

        clsobj = super().__new__(cls, clsname, bases, dict(clsdict))
        sig = make_signature(fields)
        setattr(clsobj, '__signature__', sig)
        return clsobj
