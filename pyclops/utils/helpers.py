'''
description
'''
__created__ = "2015-10-07"
__updated__ = "2015-10-07"
__author__ = "dave"
from inspect import Parameter, Signature


def make_signature(names):
    '''
    a simple function to create a signature based on a list of *args, **kwargs
    credit to David Beazley (@dabeaz) and his talk "Python 3 Meta Programming" http://www.dabeaz.com/py3meta
    '''
    return Signature(
        Parameter(name, Parameter.POSITIONAL_OR_KEYWORD)
        for name in names)
