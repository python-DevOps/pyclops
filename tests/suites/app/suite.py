'''
  This is the test suite for the pyclops.app package
'''
__updated__ = "2015-10-04"
__updated__ = "2015-10-04"
__author__ = "dave"

import unittest

from tests.utils.test_case import _test_case_loader

from . import tests


def suite_app():
    ''' Returns the unittest.TestSuite objects for each module in tests list'''
    # Modules to load TestCases from to put into the TestSuite instance
    test_cases = [test for test in _test_case_loader(tests)]
    return unittest.TestSuite(map(unittest.TestLoader().loadTestsFromModule, test_cases))
