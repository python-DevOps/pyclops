'''
  This is the test suite for all this Application related
'''
__updated__ = "2015-10-08"
__updated__ = "2015-10-04"
__author__ = "dave"

import unittest

from pyclops.app import App


class TestApplication(unittest.TestCase):
    _name = "Test Application"
    _description = "This is a test application for testing the framework"
    _expose = {"port": 80,
               "host_name": "localhost",
               "ssl": False,
               "public": False}
    _dependancies = "none"
    _additional_settings = "none"

    _new_app_data = {"name": _name,
                     "description": _description,
                     "expose": _expose,
                     "dependancies": _dependancies,
                     "settings": _additional_settings}

    def setUp(self):
        try:
            self.app = App(**self._new_app_data)
        except Exception as e:
            print(e)
            raise Exception

    def tearDown(self):
        pass

    def testValidClass(self):
        print("port:%s" % self._expose)
        assert(self.app.name == self._name)
        assert(self.app.description == self._description)
        assert(self.app.expose.port == self._expose["port"])
        assert(self.app.expose.host_name == self._expose["host_name"])
        assert(self.app.expose.public == self._expose["public"])
        assert(self.app.expose.ssl == self._expose["ssl"])
        assert(self.app.dependancies == self._dependancies)
        assert(self.app.settings == self._additional_settings)


if __name__ == "__main__":
    unittest.main()
