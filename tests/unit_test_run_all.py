'''
test_suite utils
'''
__created__ = "2015-10-04"
__updated__ = "2015-10-08"
__author__ = "dave"

import os
import sys
import unittest

sys.path.append(
    os.path.abspath(os.path.join(os.path.abspath(__file__), os.pardir, os.pardir)))

from tests import suites
from tests.utils.test_suite import _test_suite_loader


# Create the all_tests variable as a Test Suite
all_tests = unittest.TestSuite()

suites_list = _test_suite_loader(suites)

for suite in suites_list:
    all_tests.addTest(suite())

# Run the tests all together
unittest.TextTestRunner(descriptions=2, verbosity=2).run(all_tests)
