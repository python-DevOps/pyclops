'''
test_case utils
'''
__created__ = "2015-10-04"
__updated__ = "2015-10-04"
__author__ = "dave"

import importlib
import pkgutil


def _test_case_loader(package):
    test_cases = []
    for _, modname, _ in pkgutil.iter_modules(package.__path__, "%s." % package.__name__):
        module = importlib.import_module(modname)
        module_contents = [
            elem for elem in dir(module) if elem[:4] == "Test"]
        if module_contents != []:
            test_cases.append(module)

    return test_cases
