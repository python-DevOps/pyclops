'''
test_suite utils
'''
__created__ = "2015-10-04"
__updated__ = "2015-10-04"
__author__ = "dave"


import importlib
import pkgutil


def _test_suite_loader(package):
    test_suites = []
    for _, modname, _ in pkgutil.iter_modules(package.__path__, "%s." % package.__name__):
        sub_package = importlib.import_module(modname)
        for _, modname, _ in pkgutil.iter_modules(sub_package.__path__, "%s." % sub_package.__name__):
            module = importlib.import_module(modname)
            module_contents = [
                elem for elem in dir(module) if elem[:6] == "suite_"]
            for elem in module_contents:
                method = getattr(module, elem)
                if hasattr(method, '__call__'):
                    test_suites.append(method)

    return test_suites
